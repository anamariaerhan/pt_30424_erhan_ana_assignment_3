package dataAccessLayer;

import Model.Client;
import Model.Item;
import Model.OrderT;
import Model.Product;
import bussinessLayer.ReportGenerator;
import com.sun.org.apache.bcel.internal.generic.InstructionConstants;
import dao.OrderDAO;

import javax.swing.plaf.nimbus.State;
import java.sql.*;
import java.util.List;

/** Represents the class which makes the queries for several modifications in the database tables(insertions, deletions, updates).
 * @author Erhan Ana-Maria
 */
public class Operations {
    public static int indexBill = 1;
    public static int indexClientNotFound = 1;
    public static int indexProductNotFound = 1;
    public static int indexUnderStock = 1;

    /** Inserts a client in the Client table in the database.
     * @param givenName A string representing the name of the client to be added.
     * @param givenCity A string representing the city of the client to be added.
     */
    public static void insertClient(String givenName, String givenCity) {
        String insertStatementString = "insert into Client (name, city)" + " values (?, ?)";
        Connection conn = null;
        PreparedStatement insertStatement = null;

        try {
            conn = DBConnection.getConnection();
            insertStatement = conn.prepareStatement(insertStatementString);
            insertStatement.setString(1, givenName);
            insertStatement.setString(2, givenCity);
            insertStatement.executeUpdate();

            System.out.println("Insert client complete.");
        } catch(SQLException exc) {
            exc.printStackTrace();
        } finally {
            DBConnection.close(insertStatement);
            DBConnection.close(conn);
        }
    }

    /** Searches for a product in the Product table, based on its given name and
     * returns the corresponding product, if found and null, otherwise.
     * @param givenName A string representing the name of the product to be searched.
     * @return An object of type Product
     */
    public static Product findByNameProduct(String givenName) {
        String findStatementString = "SELECT * from Product where name = ?";
        Product prod = null;
        Connection con = DBConnection.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;

        try {
            findStatement = con.prepareStatement(findStatementString);
            findStatement.setString(1, givenName);
            rs = findStatement.executeQuery();

            if(rs.next()) {
                int idProduct = rs.getInt("idProduct");
                String name = givenName;
                int quantity = rs.getInt("quantity");
                double price = rs.getDouble("price");

                prod = new Product(idProduct, name, quantity, price);
            }
        } catch(SQLException exc) {
            exc.printStackTrace();
        } finally {
            DBConnection.close(rs);
            DBConnection.close(findStatement);
            DBConnection.close(con);
        }

        return prod;
    }

    /** Searches for a client in the Client table, based on its given name and
     * returns the corresponding client, if found and null, otherwise.
     * @param givenName A string representing the name of the client to be searched.
     * @return An object of type Client
     */
    public static Client findByNameClient(String givenName) { //bun
        String findStatementString = "SELECT * from Client where name = ? and deleted = 0";
        Client client = null;
        Connection con = DBConnection.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;

        try {
            findStatement = con.prepareStatement(findStatementString);
            findStatement.setString(1, givenName);
            rs = findStatement.executeQuery();

            if(rs.next()) {
                int idClient = rs.getInt("idClient");
                String name = givenName;
                String city = rs.getString("city");
                int deleted = rs.getInt("deleted");

                client = new Client(idClient, name, city, deleted);
            }
        } catch(SQLException exc) {
            exc.printStackTrace();
        } finally {
            DBConnection.close(rs);
            DBConnection.close(findStatement);
            DBConnection.close(con);
        }

        return client;
    }

    /** Updates the stock of a certain product after a successful order is made, by decreasing
     * its quantity with the taken amount.
     * @param givenProduct An object of type Product to have its stock decreased.
     * @param quantityTaken An integer representing the amount to be decreased from the whole stock of that product.
     */
    public static void decreaseProductStock(Product givenProduct, int quantityTaken) {
        givenProduct.setQuantity(givenProduct.getQuantity() - quantityTaken);
        Connection con = null;
        PreparedStatement updateStatement = null;

        try {
            con = DBConnection.getConnection();

            String updateStatementString = "UPDATE Product SET quantity = quantity - ? WHERE name = ?";
            updateStatement = con.prepareStatement(updateStatementString);
            updateStatement.setInt(1, quantityTaken);
            updateStatement.setString(2, givenProduct.getName());
            updateStatement.executeUpdate();

            System.out.println("Update product quantity complete.(decrease stock)");
        } catch(SQLException e) {
            e.printStackTrace();
        } finally {
            DBConnection.close(updateStatement);
            DBConnection.close(con);
        }
    }

    /** Inserts an item in the Item table in the database, when an order is being made.
     * @param givenClientName A string representing the name of the client who makes the order.
     * @param givenProductName A string representing the name of the product to be ordered.
     * @param givenQuantity An integer representing the quantity of the product to be ordered.
     */
    public static void insertItem(String givenClientName, String givenProductName, int givenQuantity) {
        String insertStatementString = "insert into Item (quantity, productId)" + " values (?, ?)";
        Connection conn = null;
        PreparedStatement insertStatement = null;
        ResultSet rs = null;

        try {
            conn = DBConnection.getConnection();
            Client client = findByNameClient(givenClientName);
            Product product = findByNameProduct(givenProductName);

            if(client == null) {
                ReportGenerator.clientNotFoundPDF(String.valueOf(indexClientNotFound), givenClientName);
                indexClientNotFound++;
            }
            else if(product == null) {
                ReportGenerator.clientNotFoundPDF(String.valueOf(indexProductNotFound), givenProductName);
                indexProductNotFound++;
            }
            else {
                if(product.getQuantity() >= givenQuantity) { //client who attempts to make an order exists in the database & product also exists & the stock supports the operation
                    int productId = product.getIdProduct();

                    insertStatement = conn.prepareStatement(insertStatementString, PreparedStatement.RETURN_GENERATED_KEYS);
                    insertStatement.setInt(1, givenQuantity);
                    insertStatement.setInt(2, productId);
                    insertStatement.executeUpdate();

                    System.out.println("Insert item complete.");

                    //get the id of the item just inserted, in order to insert it into order
                    int itemId = 0;
                    rs = insertStatement.getGeneratedKeys();
                    if (rs.next())
                        itemId = rs.getInt(1);

                    decreaseProductStock(product, givenQuantity);//decrease product stock
                    double totalPrice = givenQuantity * product.getPrice();
                    insertOrder(itemId, client.getIdClient(), totalPrice);//insert as order
                }
                else { //the nr of items requested exceeds the existent quantity in the stock
                    ReportGenerator.underStockPDF(String.valueOf(indexUnderStock), client.getIdClient());//generate pdf with under stock error message
                    indexUnderStock++;
                }
            }
        } catch(SQLException e) {
            e.printStackTrace();
        } finally {
            DBConnection.close(rs);
            DBConnection.close(insertStatement);
            DBConnection.close(conn);
        }
    }

    /** Inserts an order in the Order table in the database.
     * @param itemId The id of the ordered item.
     * @param clientId The id of the client who makes the order.
     * @param totalPrice A double representing the total price of the ordered products.
     */
    public static void insertOrder(int itemId, int clientId, double totalPrice) {
        String insertStatementString = "insert into OrderT (totalPrice, itemId, clientId, deleted)" + " values (?, ?, ?, ?)";
        Connection con = null;
        PreparedStatement insertStatement = null;
        ResultSet rs = null;

        try {
            con = DBConnection.getConnection();
            insertStatement = con.prepareStatement(insertStatementString, PreparedStatement.RETURN_GENERATED_KEYS);
            insertStatement.setDouble(1, totalPrice);
            insertStatement.setInt(2, itemId);
            insertStatement.setInt(3, clientId);
            insertStatement.setInt(4, 0);
            insertStatement.executeUpdate();

            //get the id of the order just inserted, in order to use it in the creation of the bill
            int orderId = 0;
            rs = insertStatement.getGeneratedKeys();
            if (rs.next())
                orderId = rs.getInt(1);
            OrderT currentOrder = new OrderT(orderId, totalPrice, itemId, clientId, 0);
            ReportGenerator.createBillPDF(String.valueOf(indexBill), currentOrder);//generate bill as pdf
            indexBill++;

            System.out.println("Insert order complete.");
        } catch(SQLException e) {
            e.printStackTrace();
        } finally {
            DBConnection.close(insertStatement);
            DBConnection.close(con);
            DBConnection.close(rs);
        }
    }

    /** Inserts a product in the Product table in the database.
     * @param givenName A string representing the name of the product to be inserted.
     * @param givenQuantity An integer representing the amount of the product to be inserted.
     * @param givenPrice A double representing the price of the product to be inserted.
     */
    public static void insertProduct(String givenName, int givenQuantity, double givenPrice) {
        Connection con = null;
        PreparedStatement insertStatement = null;
        PreparedStatement updateStatement = null;

        try {
            con = DBConnection.getConnection();

            if(findByNameProduct(givenName) != null) {//product already exists in the database, just increase the stock
                String updateStatementString = "UPDATE Product SET quantity = quantity + ? WHERE name = ? and quantity > 0";
                updateStatement = con.prepareStatement(updateStatementString);
                updateStatement.setInt(1, givenQuantity);
                updateStatement.setString(2, givenName);
                updateStatement.executeUpdate();

                System.out.println("Update product quantity complete.");
            }
            else { //new product to be added
                String insertStatementString = "insert into Product (name, quantity, price)" + " values (?, ?, ?)";
                insertStatement = con.prepareStatement(insertStatementString);
                insertStatement.setString(1, givenName);
                insertStatement.setInt(2, givenQuantity);
                insertStatement.setDouble(3, givenPrice);
                insertStatement.executeUpdate();

                System.out.println("Insert product complete.");
            }
        } catch(SQLException e) {
            e.printStackTrace();
        } finally {
            DBConnection.close(insertStatement);
            DBConnection.close(updateStatement);
            DBConnection.close(con);
        }
    }

    /** Deletes an order in the database(not by using the delete query, but instead
     * updating the deleted flag to 1, so that the status of the order is now deleted).
     * @param givenOrder An object of type OrderT, which will be marked as deleted.
     * @param clientId An integer representing the id of the client who made that order.
     */
    public static void deleteOrder(OrderT givenOrder, int clientId) {
        Connection con = null;
        PreparedStatement updateStatement = null;

        try {
            con = DBConnection.getConnection();

            String updateStatementString = "UPDATE OrderT SET deleted = 1 WHERE clientId = ?";
            updateStatement = con.prepareStatement(updateStatementString);
            updateStatement.setInt(1, clientId);
            updateStatement.executeUpdate();
            System.out.println("Delete order complete.");
        } catch(SQLException e) {
            e.printStackTrace();
        } finally {
            DBConnection.close(updateStatement);
            DBConnection.close(con);
        }
    }

    /** Deletes a client in the database(not by using the delete query, but instead
     * updating the deleted flag to 1, so that the status of the client is now deleted).
     * @param givenName A String representing the name of the client which will be marked as deleted.
     * @param givenCity A String representing the city of the client which will be marked as deleted.
     */
    public static void deleteClient(String givenName, String givenCity) { //bun
        Client toBeDeleted = findByNameClient(givenName);
        if(toBeDeleted != null && toBeDeleted.getDeleted() != 1) { //check if given client exists in the database & the client was not already deleted
            //check all the client's orders and delete them
            List<OrderT> ordersList = OrderDAO.getOrders();//get all the non-deleted orders
            for (OrderT currentOrder: ordersList) {
                if(currentOrder.getClientId() == toBeDeleted.getIdClient()) {//the order belongs to the client to be deleted
                    currentOrder.setDeleted(1);//mark order as deleted
                    deleteOrder(currentOrder, toBeDeleted.getIdClient());//delete Order
                }
            }
            //delete client
            Connection con = null;
            PreparedStatement updateStatement = null;

            try {
                con = DBConnection.getConnection();

                String updateStatementString = "UPDATE Client SET deleted = 1 WHERE name = ? and city = ?";
                updateStatement = con.prepareStatement(updateStatementString);
                updateStatement.setString(1, givenName);
                updateStatement.setString(2, givenCity);
                updateStatement.executeUpdate();
                System.out.println("Delete client complete.");
            } catch(SQLException e) {
                e.printStackTrace();
            } finally {
                DBConnection.close(updateStatement);
                DBConnection.close(con);
            }
            toBeDeleted.setDeleted(1);//mark client as deleted
        }
    }

    /** Deletes a product in the database.
     * @param givenName A String representing the name of the product which will be deleted.
     */
    public static void deleteProduct(String givenName) { //bun
        Product toBeDeleted = findByNameProduct(givenName);
        if(toBeDeleted != null) {
            Connection con = DBConnection.getConnection();
            PreparedStatement deleteStatement = null;
            String deleteStatementString = "DELETE from Product where name = ?";

            try {
                deleteStatement = con.prepareStatement(deleteStatementString);
                deleteStatement.setString(1, givenName);
                deleteStatement.executeUpdate();
                System.out.println("Delete product complete.");
            } catch (SQLException exc) {
                exc.printStackTrace();
            } finally {
                DBConnection.close(deleteStatement);
                DBConnection.close(con);
            }
        }
    }
}
