package Model;

/** Represents an order.
 * @author Erhan Ana-Maria
 */
public class OrderT {
    private int idOrder;
    private double totalPrice;
    private int itemId;
    private int clientId;
    private int deleted;

    /** Creates an order.
     */
    public OrderT() {
    }

    /** Creates an order with the specified attributes(id included)
     * @param idOrder The order’s id.
     * @param totalPrice The order’s total price.
     * @param itemId The id of the item to be ordered.
     * @param clientId The id of the client who placed the order.
     * @param deleted The deleted status of the order.
     */
    public OrderT(int idOrder, double totalPrice, int itemId, int clientId, int deleted) {
        super();
        this.idOrder = idOrder;
        this.totalPrice = totalPrice;
        this.itemId = itemId;
        this.clientId = clientId;
        this.deleted = deleted;
    }

    /** Creates an order with the specified attributes(id not included)
     * @param totalPrice The order’s total price.
     * @param itemId The id of the item to be ordered.
     * @param clientId The id of the client who placed the order.
     * @param deleted The deleted status of the order.
     */
    public OrderT(double totalPrice, int itemId, int clientId, int deleted) {
        super();
        this.totalPrice = totalPrice;
        this.itemId = itemId;
        this.clientId = clientId;
        this.deleted = deleted;
    }

    /** Gets the order’s deleted flag status.
     * @return An integer representing the order’s deleted status.
     */
    public int getDeleted() {
        return deleted;
    }

    /** Sets the order’s deleted flag status.
     * @param deleted An integer containing the order's deleted status.
     */
    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    /** Gets the client’s id.
     * @return An integer representing the client's id.
     */
    public int getClientId() {
        return clientId;
    }

    /** Sets the client’s id.
     * @param clientId An integer containing the
     *     client’s id.
     */
    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    /** Gets the item’s id.
     * @return An integer representing the item's id.
     */
    public int getItemId() {
        return itemId;
    }

    /** Sets the item’s id.
     * @param itemId An integer containing the
     *     item’s id.
     */
    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    /** Gets the order’s id.
     * @return An integer representing the order's id.
     */
    public int getIdOrder() {
        return idOrder;
    }

    /** Sets the order’s id.
     * @param idOrder An integer containing the
     *     order’s id.
     */
    public void setIdOrder(int idOrder) {
        this.idOrder = idOrder;
    }

    /** Gets the order’s total price.
     * @return An integer representing the order's total price.
     */
    public double getTotalPrice() {
        return totalPrice;
    }

    /** Sets the order’s total price.
     * @param totalPrice A double containing the
     *     order’s total price.
     */
    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    /** Creates a string representation of all the order's attributes.
     * @return A string representing the order's attributes.
     */
    @Override
    public String toString() {
        return "Order [id = " + this.idOrder + ", total price = " + this.totalPrice + "]";
    }
}
