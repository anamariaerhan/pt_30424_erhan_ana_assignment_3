package Model;

/** Represents an item.
 * @author Erhan Ana-Maria
 */
public class Product {
    private int idProduct;
    private String name;
    private int quantity;
    private double price;

    /** Creates a product.
     */
    public Product() {
    }

    /** Creates an product with the specified attributes(id included)
     * @param idProduct The product’s id.
     * @param name The product's name.
     * @param quantity The product’s quantity.
     * @param price The product's price.
     */
    public Product(int idProduct, String name, int quantity, double price) {
        super();
        this.idProduct = idProduct;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
    }

    /** Creates an product with the specified attributes(id not included)
     * @param name The product's name.
     * @param quantity The product’s quantity.
     * @param price The product's price.
     */
    public Product(String name, int quantity, double price) {
        super();
        this.name = name;
        this.quantity = quantity;
        this.price = price;
    }

    /** Gets the product’s id.
     * @return An integer representing the product's id.
     */
    public int getIdProduct() {
        return idProduct;
    }

    /** Sets the product’s id.
     * @param idProduct An integer containing the
     *     product’s id.
     */
    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    /** Gets the product’s name.
     * @return A string representing the product's name.
     */
    public String getName() {
        return name;
    }

    /** Sets the product’s full name.
     * @param name A String containing the
     *     product’s name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /** Gets the product’s quantity.
     * @return An integer representing the product's quantity.
     */
    public int getQuantity() {
        return quantity;
    }

    /** Sets the product’s quantity.
     * @param quantity An integer containing the
     *     product’s quantity.
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /** Gets the product’s price.
     * @return An integer representing the product's price.
     */
    public double getPrice() {
        return price;
    }

    /** Sets the product’s price.
     * @param price A double containing the
     *     product’s price.
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /** Creates a string representation of all the product's attributes.
     * @return A string representing the product's attributes.
     */
    @Override
    public String toString() {
        return "Product [id = " + this.idProduct + ", name = " + this.name + ", quantity = " + this.quantity + ", price =" + this.price + "]";
    }
}
