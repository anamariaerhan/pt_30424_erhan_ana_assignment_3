package Model;

/** Represents a client.
 * @author Erhan Ana-Maria
 */
public class Client {
    private int idClient;
    private String name;
    private String city;
    private int deleted;

    /** Creates a client.
     */
    public Client() {
    }

    /** Creates a client with the specified attributes(id included)
     * @param idClient The client’s id.
     * @param name The client’s full name.
     * @param city The client's city.
     * @param deleted The client's deleted status(flag)
     */
    public Client(int idClient, String name, String city, int deleted) {
        super();
        this.idClient = idClient;
        this.name = name;
        this.city = city;
        this.deleted = deleted;
    }

    /** Creates a client with the specified attributes(without id)
     * @param name The client’s full name.
     * @param city The client's city.
     * @param deleted The client's deleted status(flag), 0 - deleted, 1 - not deleted
     */
    public Client(String name, String city, int deleted) {
        super();
        this.name = name;
        this.city = city;
        this.deleted = deleted;
    }

    /** Gets the client’s deleted flag status.
     * @return An integer representing the client’s deleted status.
     */
    public int getDeleted() {
        return deleted;
    }

    /** Sets the client’s deleted flag status.
     * @param deleted An integer containing the client's deleted status.
     */
    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    /** Gets the client’s id.
     * @return An integer representing the client's id.
     */
    public int getIdClient() {
        return idClient;
    }

    /** Sets the client’s id.
     * @param idClient An integer containing the
     *     client’s id.
     */
    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    /** Gets the client’s full name.
     * @return A string representing the client's name.
     */
    public String getName() {
        return name;
    }

    /** Sets the client’s full name.
     * @param name A String containing the
     *     client’s name.
     */
    public void setName(String name) {
        this.name = name;
    }

    /** Gets the client’s city.
     * @return A string representing the client's city.
     */
    public String getCity() {
        return city;
    }

    /** Sets the client’s city.
     * @param city A String containing the
     *     client’s city.
     */
    public void setCity(String city) {
        this.city = city;
    }

    /** Creates a string representation of all the client's attributes.
     * @return A string representing the client's attributes.
     */
    @Override
    public String toString() {
        return "Client [id = " + this.idClient + ", name = " +
                this.name + ", city =" + this.city + "]";
    }
}
