package Model;

/** Represents an item.
 * @author Erhan Ana-Maria
 */
public class Item {
    private int idItem;
    private int quantity;
    private int productId;

    /** Creates an item.
     */
    public Item() {
    }

    /** Creates an item with the specified attributes(id included)
     * @param idItem The item’s id.
     * @param quantity The item’s quantity.
     * @param productId The id of the product of which the item is selected as when making an order.
     */
    public Item(int idItem, int quantity, int productId) {
        super();
        this.idItem = idItem;
        this.quantity = quantity;
        this.productId = productId;
    }

    /** Creates an item with the specified attributes(id not included)
     * @param quantity The item’s quantity.
     * @param productId The id of the product of which the item is selected as when making an order.
     */
    public Item(int quantity, int productId) {
        super();
        this.quantity = quantity;
        this.productId = productId;
    }

    /** Gets the product’s id for the item to be saved when placing an order.
     * @return An integer representing the product's id.
     */
    public int getProductId() {
        return productId;
    }

    /** Sets the product’s id.
     * @param productId An integer containing the
     *     product’s id.
     */
    public void setProductId(int productId) {
        this.productId = productId;
    }

    /** Gets the item’s id.
     * @return An integer representing the item's id.
     */
    public int getIdItem() {
        return idItem;
    }

    /** Sets the item’s id.
     * @param idItem An integer containing the
     *     item’s id.
     */
    public void setIdItem(int idItem) {
        this.idItem = idItem;
    }

    /** Gets the item’s quantity.
     * @return An integer representing the item's quantity.
     */
    public int getQuantity() {
        return quantity;
    }

    /** Sets the item’s quantity.
     * @param quantity An integer containing the
     *     item’s quantity.
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /** Creates a string representation of all the item's attributes.
     * @return A string representing the item's attributes.
     */
    @Override
    public String toString() {
        return "Item [id = " + this.idItem + ", quantity = " + this.quantity + "]";
    }
}
