package Presentation;

import Model.Client;
import bussinessLayer.ReportGenerator;
import dataAccessLayer.DBConnection;
import dataAccessLayer.Operations;

import java.io.*;
import java.nio.file.*;

/** Represents the main class which simulates the entire order management application.
 * @author Erhan Ana-Maria
 */
public class OrderManagement {
    /**
     * Parser for processing the requested commands.
     *
     * @param args command-line arguments
     */
    public static void main(String[] args) {
        OrderManagement myOrderManagement = new OrderManagement();

        try {
            File myFile = new File(args[0]);
            FileReader fr = new FileReader(myFile);
            BufferedReader br = new BufferedReader(fr);
            String currentLine;
            ReportGenerator report = new ReportGenerator();
            int indexProduct = 1, indexOrder = 1, indexClient = 1;
            while ((currentLine = br.readLine()) != null) {
                if(currentLine.startsWith("Insert")) {
                    if(currentLine.indexOf("client") != -1) { //check for client
                        //insert client
                        int indexColon = currentLine.indexOf(':');
                        int indexComma = currentLine.indexOf(',');
                        String clientName = currentLine.substring(indexColon + 2, indexComma);
                        String clientCity = currentLine.substring(indexComma + 2, currentLine.length());
                        Operations.insertClient(clientName, clientCity);
                    }
                    else if(currentLine.indexOf("product") != -1) { //check for product
                        //insert product
                        int indexColon = currentLine.indexOf(':');
                        int indexComma = currentLine.indexOf(',');
                        String productName = currentLine.substring(indexColon + 2, indexComma);
                        String restOfLine = currentLine.substring(indexComma + 2, currentLine.length());
                        indexComma = restOfLine.indexOf(',');
                        int productQuantity = Integer.parseInt(restOfLine.substring(0, indexComma));
                        double productPrice = Double.parseDouble(restOfLine.substring(indexComma + 2, restOfLine.length()));
                        Operations.insertProduct(productName, productQuantity, productPrice);
                    }
                }
                else if(currentLine.startsWith("Delete")) {
                    if(currentLine.indexOf("client") != -1) { //check for client
                        //delete client
                        int indexColon = currentLine.indexOf(':');
                        int indexComma = currentLine.indexOf(',');
                        String clientName = currentLine.substring(indexColon + 2, indexComma);
                        String clientCity = currentLine.substring(indexComma + 2, currentLine.length());
                        Operations.deleteClient(clientName, clientCity);
                    }
                    else if(currentLine.indexOf("Product") != -1) { //check for product
                        //delete product
                        int indexColon = currentLine.indexOf(':');
                        String productName = currentLine.substring(indexColon + 2, currentLine.length());
                        Operations.deleteProduct(productName);
                    }
                }
                else if(currentLine.startsWith("Order")) {
                    //add new item
                    int indexColon = currentLine.indexOf(':');
                    int indexComma = currentLine.indexOf(',');
                    String clientName = currentLine.substring(indexColon + 2, indexComma);
                    String restOfLine = currentLine.substring(indexComma + 2, currentLine.length());
                    indexComma = restOfLine.indexOf(',');
                    String productName = restOfLine.substring(0, indexComma);
                    int quantity = Integer.parseInt(restOfLine.substring(indexComma + 2, restOfLine.length()));
                    Operations.insertItem(clientName, productName, quantity);
                }
                else if(currentLine.startsWith("Report")) {
                    if(currentLine.indexOf("client") != -1) { //check for client
                        //Generate pdf report for client
                        report.createPDFClient(String.valueOf(indexClient));
                        indexClient++;
                    }
                    else if(currentLine.indexOf("product") != -1) { //check for product
                        //Generate pdf report for product
                        report.createPDFProduct(String.valueOf(indexProduct));
                        indexProduct++;
                    }
                    else if(currentLine.indexOf("order") != -1) { //check for order
                        //Generate pdf report for order
                        report.createPDFOrder(String.valueOf(indexOrder));
                        indexOrder++;
                    }
                }
            }

            fr.close();
        } catch (IOException exc) {
            exc.printStackTrace();
        }
    }
}
