package dao;

import Model.Product;
import com.sun.org.apache.xpath.internal.operations.Or;
import dataAccessLayer.DBConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/** Represents the class which performs a selection of all the products found in the database
 * and adds them to a list.
 * @author Erhan Ana-Maria
 */
public class ProductDAO {
    private static Connection conn;
    private static Statement statement;

    /** Creates a ProductDAO.
     */
    public ProductDAO() {
    }

    /** Retrieves all the information related to all the products in the database.
     * @return A list of objects of type Product.
     */
    public static List<Product> getProducts() {
        String findStatementString = "SELECT * from Product";
        List<Product> productsList = new ArrayList<Product>();
        Product currentProduct = null;
        ResultSet rs = null;

        try {
            conn = DBConnection.getConnection();
            statement = conn.createStatement();
            rs = statement.executeQuery(findStatementString);

            while(rs.next()) {
                currentProduct = new Product();
                currentProduct.setIdProduct(rs.getInt("idProduct"));
                currentProduct.setName(rs.getString("name"));
                currentProduct.setQuantity(rs.getInt("quantity"));
                currentProduct.setPrice(rs.getDouble("price"));

                productsList.add(currentProduct);
            }
        } catch(SQLException exc) {
            exc.printStackTrace();
        } finally {
            DBConnection.close(rs);
            DBConnection.close(statement);
            DBConnection.close(conn);
        }

        return productsList;
    }
}

