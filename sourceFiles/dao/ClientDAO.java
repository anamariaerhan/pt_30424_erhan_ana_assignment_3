package dao;

import Model.Client;
import com.sun.org.apache.xpath.internal.operations.Or;
import dataAccessLayer.DBConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/** Represents the class which performs a selection of all the clients found in the database
 * and adds them to a list.
 * @author Erhan Ana-Maria
 */
public class ClientDAO {
    private static Connection conn;
    private static Statement statement;

    /** Creates a ClientDAO.
     */
    public ClientDAO() {
    }

    /** Retrieves all the information related to all the clients in the database.
     * @return A list of objects of type Client.
     */
    public static List<Client> getClients() {
        String findStatementString = "SELECT * from Client WHERE deleted = 0";//get only non-deleted clients
        List<Client> clientsList = new ArrayList<Client>();
        Client currentClient = null;
        ResultSet rs = null;

        try {
            conn = DBConnection.getConnection();
            statement = conn.createStatement();
            rs = statement.executeQuery(findStatementString);

            while(rs.next()) {
                currentClient = new Client();
                currentClient.setIdClient(rs.getInt("idClient"));
                currentClient.setName(rs.getString("name"));
                currentClient.setCity(rs.getString("city"));
                currentClient.setDeleted(0);//0 anyway, as the condition is put in the select statement

                clientsList.add(currentClient);
            }
        } catch(SQLException exc) {
            exc.printStackTrace();
        } finally {
            DBConnection.close(rs);
            DBConnection.close(statement);
            DBConnection.close(conn);
        }

        return clientsList;
    }
}

