package dao;

import Model.OrderT;
import com.sun.org.apache.xpath.internal.operations.Or;
import dataAccessLayer.DBConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/** Represents the class which performs a selection of all the orders found in the database
 * and adds them to a list.
 * @author Erhan Ana-Maria
 */
public class OrderDAO {
    private static Connection conn;
    private static Statement statement;

    /** Creates an OrderDAO.
     */
    public OrderDAO() {
    }

    /** Retrieves all the information related to all the orders in the database.
     * @return A list of objects of type OrderT.
     */
    public static List<OrderT> getOrders() {
        String findStatementString = "SELECT * from OrderT WHERE deleted = 0";//get only non-deleted orders
        List<OrderT> ordersList = new ArrayList<OrderT>();
        OrderT currentOrder = null;
        ResultSet rs = null;

        try {
            conn = DBConnection.getConnection();
            statement = conn.createStatement();
            rs = statement.executeQuery(findStatementString);

            while(rs.next()) {
                currentOrder = new OrderT();
                currentOrder.setIdOrder(rs.getInt("idOrder"));
                currentOrder.setTotalPrice(rs.getDouble("totalPrice"));
                currentOrder.setItemId(rs.getInt("itemId"));
                currentOrder.setClientId(rs.getInt("clientId"));
                currentOrder.setDeleted(0);//0 anyway, as the condition is put in the select statement

                ordersList.add(currentOrder);
            }
        } catch(SQLException exc) {
            exc.printStackTrace();
        } finally {
            DBConnection.close(rs);
            DBConnection.close(statement);
            DBConnection.close(conn);
        }

        return ordersList;
    }
}
