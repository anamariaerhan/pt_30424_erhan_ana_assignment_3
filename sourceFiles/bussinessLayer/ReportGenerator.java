package bussinessLayer;

import Model.Client;
import Model.OrderT;
import Model.Product;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import dao.ClientDAO;
import dao.OrderDAO;
import dao.ProductDAO;

import java.io.FileOutputStream;
import java.text.*;
import java.util.List;

/** Represents the class which generates and handles PDF files for reports and bills.
 * @author Erhan Ana-Maria
 */
public class ReportGenerator {
        /** Creates a PDF file containing all the products found in the database in tabular form at the moment of
         * the simulation in which the requested command is "Report product".
         * @param pdfIndex A string containing the
         *     index of the current requested report.
         */
        public static void createPDFProduct(String pdfIndex) {
            Document doc = new Document();
            PdfWriter docWriter = null;

            try {
                //font sizes
                Font bfBold12 = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD, new BaseColor(0, 0, 0));
                Font bf12 = new Font(Font.FontFamily.TIMES_ROMAN, 12);

                String path = "ReportProduct" + pdfIndex + ".pdf";
                docWriter = PdfWriter.getInstance(doc, new FileOutputStream(path));

                //set header attributes
                doc.addAuthor("Erhan Ana-Maria");
                doc.addTitle("Report product");
                doc.setPageSize(PageSize.LETTER);

                doc.open();

                Paragraph paragraph = new Paragraph("Generated product report:");

                //specify column widths
                float[] columnWidths = {1.5f, 2f, 2f, 2f};
                //create PDF table with the given widths
                PdfPTable table = new PdfPTable(columnWidths);
                // set table width a percentage of the page width
                table.setWidthPercentage(90f);

                //insert column headings
                insertCell(table, "idProduct", Element.ALIGN_CENTER, 1, bfBold12);
                insertCell(table, "name", Element.ALIGN_CENTER, 1, bfBold12);
                insertCell(table, "quantity", Element.ALIGN_CENTER, 1, bfBold12);
                insertCell(table, "price", Element.ALIGN_CENTER, 1, bfBold12);
                table.setHeaderRows(1);

                List<Product> productsList = ProductDAO.getProducts();//get all the current products in the database

                //fill table with corresponding data
                for(Product currentProduct: productsList) {
                    insertCell(table, String.valueOf(currentProduct.getIdProduct()), Element.ALIGN_CENTER, 1, bf12);
                    insertCell(table, currentProduct.getName(), Element.ALIGN_CENTER, 1, bf12);
                    insertCell(table, String.valueOf(currentProduct.getQuantity()), Element.ALIGN_CENTER, 1, bf12);
                    insertCell(table, String.valueOf(currentProduct.getPrice()), Element.ALIGN_CENTER, 1, bf12);
                }

                paragraph.add(table);//add the PDF table to the paragraph
                doc.add(paragraph);//add the paragraph to the document
            } catch (DocumentException dex) {
                dex.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                doc.close();
                docWriter.close();
            }
        }

        /** Creates a PDF file containing all the clients found in the database in tabular form at the moment of
         * the simulation in which the requested command is "Report clients".
         * @param pdfIndex A string containing the
         *     index of the current requested report.
         */
        public static void createPDFClient(String pdfIndex) {
            Document doc = new Document();
            PdfWriter docWriter = null;

            try {
                Font bfBold12 = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD, new BaseColor(0, 0, 0));
                Font bf12 = new Font(Font.FontFamily.TIMES_ROMAN, 12);

                String path = "ReportClient" + pdfIndex + ".pdf";
                docWriter = PdfWriter.getInstance(doc, new FileOutputStream(path));

                //set header attributes
                doc.addAuthor("Erhan Ana-Maria");
                doc.addTitle("Report client");
                doc.setPageSize(PageSize.LETTER);

                doc.open();

                Paragraph paragraph = new Paragraph("Generated client report:");

                float[] columnWidths = {1.5f, 2f, 2f, 2f};
                PdfPTable table = new PdfPTable(columnWidths);
                table.setWidthPercentage(90f);

                insertCell(table, "idClient", Element.ALIGN_CENTER, 1, bfBold12);
                insertCell(table, "name", Element.ALIGN_CENTER, 1, bfBold12);
                insertCell(table, "city", Element.ALIGN_CENTER, 1, bfBold12);
                insertCell(table, "deleted", Element.ALIGN_CENTER, 1, bfBold12);
                table.setHeaderRows(1);

                List<Client> clientsList = ClientDAO.getClients();//get all the current clients in the database

                for(Client currentClient: clientsList) {
                    insertCell(table, String.valueOf(currentClient.getIdClient()), Element.ALIGN_CENTER, 1, bf12);
                    insertCell(table, currentClient.getName(), Element.ALIGN_CENTER, 1, bf12);
                    insertCell(table, currentClient.getCity(), Element.ALIGN_CENTER, 1, bf12);
                    insertCell(table, String.valueOf(currentClient.getDeleted()), Element.ALIGN_CENTER, 1, bf12);
                }

                paragraph.add(table);
                doc.add(paragraph);
            } catch (DocumentException dex) {
                dex.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                doc.close();
                docWriter.close();
            }
        }

        /** Creates a PDF file containing all the orders found in the database in tabular form at the moment of
         * the simulation in which the requested command is "Report orders".
         * @param pdfIndex A string containing the
         *     index of the current requested report.
         */
        public static void createPDFOrder(String pdfIndex) {
            Document doc = new Document();
            PdfWriter docWriter = null;

            try {
                Font bfBold12 = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD, new BaseColor(0, 0, 0));
                Font bf12 = new Font(Font.FontFamily.TIMES_ROMAN, 12);

                String path = "ReportOrder" + pdfIndex + ".pdf";
                docWriter = PdfWriter.getInstance(doc, new FileOutputStream(path));

                //set header attributes
                doc.addAuthor("Erhan Ana-Maria");
                doc.addTitle("Report order");
                doc.setPageSize(PageSize.LETTER);

                doc.open();

                Paragraph paragraph = new Paragraph("Generated order report:");

                float[] columnWidths = {1.5f, 2f, 2f, 2f, 2f};
                PdfPTable table = new PdfPTable(columnWidths);
                table.setWidthPercentage(90f);

                insertCell(table, "idOrder", Element.ALIGN_CENTER, 1, bfBold12);
                insertCell(table, "totalPrice", Element.ALIGN_CENTER, 1, bfBold12);
                insertCell(table, "itemId", Element.ALIGN_CENTER, 1, bfBold12);
                insertCell(table, "clientId", Element.ALIGN_CENTER, 1, bfBold12);
                insertCell(table, "deleted", Element.ALIGN_CENTER, 1, bfBold12);
                table.setHeaderRows(1);

                List<OrderT> ordersList = OrderDAO.getOrders();//get all the current orders in the database

                for(OrderT currentOrder: ordersList) {
                    insertCell(table, String.valueOf(currentOrder.getIdOrder()), Element.ALIGN_CENTER, 1, bf12);
                    insertCell(table, String.valueOf(currentOrder.getTotalPrice()), Element.ALIGN_CENTER, 1, bf12);
                    insertCell(table, String.valueOf(currentOrder.getItemId()), Element.ALIGN_CENTER, 1, bf12);
                    insertCell(table, String.valueOf(currentOrder.getClientId()), Element.ALIGN_CENTER, 1, bf12);
                    insertCell(table, String.valueOf(currentOrder.getDeleted()), Element.ALIGN_CENTER, 1, bf12);
                }

                paragraph.add(table);
                doc.add(paragraph);
            } catch (DocumentException dex) {
                dex.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                doc.close();
                docWriter.close();
            }
        }

        /** Creates a PDF file containing a bill for the newly inserted order in tabular form at the moment of
         * the simulation in which the requested command is "Insert order".
         * @param pdfIndex A string containing the
         *     index of the current requested report.
         */
        public static void createBillPDF(String pdfIndex, OrderT currentOrder) {
            Document doc = new Document();
            PdfWriter docWriter = null;

            try {
                Font bfBold12 = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD, new BaseColor(0, 0, 0));
                Font bf12 = new Font(Font.FontFamily.TIMES_ROMAN, 12);

                String path = "Bill" + pdfIndex + ".pdf";
                docWriter = PdfWriter.getInstance(doc, new FileOutputStream(path));
                doc.open();

                Paragraph paragraph = new Paragraph("Generated bill:");

                float[] columnWidths = {1.5f, 2f, 2f, 2f, 2f};
                PdfPTable table = new PdfPTable(columnWidths);
                table.setWidthPercentage(90f);

                insertCell(table, "idOrder", Element.ALIGN_CENTER, 1, bfBold12);
                insertCell(table, "totalPrice", Element.ALIGN_CENTER, 1, bfBold12);
                insertCell(table, "itemId", Element.ALIGN_CENTER, 1, bfBold12);
                insertCell(table, "clientId", Element.ALIGN_CENTER, 1, bfBold12);
                insertCell(table, "deleted", Element.ALIGN_CENTER, 1, bfBold12);
                table.setHeaderRows(1);

                insertCell(table, String.valueOf(currentOrder.getIdOrder()), Element.ALIGN_CENTER, 1, bf12);
                insertCell(table, String.valueOf(currentOrder.getTotalPrice()), Element.ALIGN_CENTER, 1, bf12);
                insertCell(table, String.valueOf(currentOrder.getItemId()), Element.ALIGN_CENTER, 1, bf12);
                insertCell(table, String.valueOf(currentOrder.getClientId()), Element.ALIGN_CENTER, 1, bf12);
                insertCell(table, String.valueOf(currentOrder.getDeleted()), Element.ALIGN_CENTER, 1, bf12);

                paragraph.add(table);
                doc.add(paragraph);
            } catch (DocumentException dex) {
                dex.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                doc.close();
                docWriter.close();
            }
        }

        /** Creates a PDF file containing an error message when the newly requested order cannot be placed
         * bacause the product stock does not comply to the requested quantity at the command "Insert order".
         * @param pdfIndex A string containing the
         *     index of the current requested report.
         */
        public static void underStockPDF(String pdfIndex, int clientId) {
            Document doc = new Document();
            PdfWriter docWriter = null;

            try {
                Font bfBold12 = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD, new BaseColor(0, 0, 0));
                Font bf12 = new Font(Font.FontFamily.TIMES_ROMAN, 12);

                String path = "UnderStock" + pdfIndex + ".pdf";
                docWriter = PdfWriter.getInstance(doc, new FileOutputStream(path));
                doc.open();

                Paragraph paragraph = new Paragraph("Error message : insufficient stock for client with id " + clientId);
                doc.add(paragraph);
            } catch (DocumentException dex) {
                dex.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                doc.close();
                docWriter.close();
            }
        }

        /** Creates a PDF file containing an error message when the newly requested order cannot be placed
         * bacause the the client to order is not found in the database, at the command "Insert order".
         * @param pdfIndex A string containing the
         *     index of the current requested report.
         */
        public static void clientNotFoundPDF(String pdfIndex, String givenClientName) {
            Document doc = new Document();
            PdfWriter docWriter = null;

            try {
                Font bfBold12 = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD, new BaseColor(0, 0, 0));
                Font bf12 = new Font(Font.FontFamily.TIMES_ROMAN, 12);

                String path = "ClientNotFound" + pdfIndex + ".pdf";
                docWriter = PdfWriter.getInstance(doc, new FileOutputStream(path));
                doc.open();

                Paragraph paragraph = new Paragraph("Error message : client " + givenClientName + " not found");
                doc.add(paragraph);
            } catch (DocumentException dex) {
                dex.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                doc.close();
                docWriter.close();
            }
        }

        /** Creates a PDF file containing an error message when the newly requested order cannot be placed
         * bacause the the product ordered is not found in the database, at the command "Insert order".
         * @param pdfIndex A string containing the
         *     index of the current requested report.
         */
        public static void productNotFoundPDF(String pdfIndex, String givenProductName) {
            Document doc = new Document();
            PdfWriter docWriter = null;

            try {
                Font bfBold12 = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD, new BaseColor(0, 0, 0));
                Font bf12 = new Font(Font.FontFamily.TIMES_ROMAN, 12);

                String path = "ProductNotFound" + pdfIndex + ".pdf";
                docWriter = PdfWriter.getInstance(doc, new FileOutputStream(path));
                doc.open();

                Paragraph paragraph = new Paragraph("Error message : product " + givenProductName + " not found");
                doc.add(paragraph);
            } catch (DocumentException dex) {
                dex.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                doc.close();
                docWriter.close();
            }
        }

        /** Inserts a cell in the table contained in the PDF file generated at the commands regarding
         * reports or the command "Insert order".
         * @param table The table in which the cell will be added.
         * @param text A string representing the text of the cell.
         * @param align An integer representing the alignment bound of the cell.
         * @param colspan An integer representing the column span in the table.
         * @param font Represents the font of the text in the cell.
         */
        private static void insertCell(PdfPTable table, String text, int align, int colspan, Font font) {
            PdfPCell cell = new PdfPCell(new Phrase(text.trim(), font));//create a new cell with the specified Text and Font
            cell.setHorizontalAlignment(align);
            cell.setColspan(colspan);//set the cell column span in case you want to merge two or more cells

            if(text.trim().equalsIgnoreCase("")) //in case there is no text and you want to create an empty row
                cell.setMinimumHeight(10f);

            table.addCell(cell);
        }
}
